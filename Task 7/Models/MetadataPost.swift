//
//  MetadataPost.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 19.03.22.
//

import Foundation

struct MetadataPost: Codable {
    var form: [String: String?]
}
