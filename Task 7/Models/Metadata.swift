//
//  Metadata.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import Foundation

// MARK: - Metadata
struct Metadata: Codable {
    let title: String
    let image: String
    var fields: [Field]
}

// MARK: - Field
extension Metadata {
    enum TypeField: String, Codable {
        case text = "TEXT"
        case numeric = "NUMERIC"
        case list = "LIST"
    }
    struct Field: Codable {
        let title: String
        let name: String
        let type: TypeField
        private let values: [String: String]?
        var indexChasedValue: Int?
        var answer: String?
    }
}

extension Metadata.Field {
    var sortedValues: [String]? {
        values?.sorted(by: { $0.key < $1.key }).map({ $0.value })
    }
    var chasedValue: String? {
        guard let sortedValues = sortedValues else {
            return nil
        }
        if let indexChasedValue = indexChasedValue {
            return sortedValues[indexChasedValue]
        }
        return sortedValues[0]
    }
}

// MARK: - RequestableMetadata
extension Metadata: RequestableMetadata {
    static var urlRequest: URL {
        return URL(string: "http://test.clevertec.ru/tt/meta/")!
    }
}
