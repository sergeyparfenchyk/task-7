//
//  MetadataAnswer.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import Foundation

struct MetadataAnswer: Codable {
    let result: String
}

// MARK: - ResponsibleMetadata
extension MetadataAnswer: ResponsibleMetadata {
    static var urlRequest: URLRequest {
        let url = URL(string: "http://test.clevertec.ru/tt/data/")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        return request
    }
}
