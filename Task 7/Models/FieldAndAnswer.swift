//
//  FieldAndAnswer.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 21.03.22.
//

import UIKit

struct FieldAndAnswer {
    var field: ListMetadata.Field
    var color: UIColor
}
