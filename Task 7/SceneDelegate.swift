//
//  SceneDelegate.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow.init(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = createRootViewController()
        window?.makeKeyAndVisible()
    }

    // MARK: - Create Root ViewControlle
    private func createRootViewController() -> UIViewController {
        let containerViewControllers = ListMetadataViewController()
        let navigationViewController = UINavigationController(rootViewController: containerViewControllers)
        return navigationViewController
    }
}
