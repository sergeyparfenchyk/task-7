//
//  MonitorNetworkWorker.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 21.03.22.
//

import UIKit
import Network

class MonitorNetworkWorker {
    static func checkInternet(result: @escaping (Bool) -> Void) {
        let monitor = NWPathMonitor()
        let queue = DispatchQueue.global(qos: .userInteractive)
        monitor.pathUpdateHandler = { pathUpdateHandler in
            let isInternet = pathUpdateHandler.status == .satisfied ? true : false
            DispatchQueue.main.async {
                result(isInternet)
            }
            monitor.cancel()
        }
        monitor.start(queue: queue)
    }
}
