//
//  ListMetadataViewController.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit
import SnapKit

// MARK: - Constants

private extension ConstantString {
    static let contactTableViewCellId = "CellID"
    static let sendImageButton = "Send"
}

private extension ConstantColor {
    static let sendButton = UIColor.systemGray6
}

// MARK: - Protocols

protocol ListMetadataDisplayLogic: AnyObject {
    func displayFetch(viewModel: ListMetadata.Fetch.ViewModel)
    func displayUpdate(viewModel: ListMetadata.Update.ViewModel)
}

// MARK: - ListMetadataViewController
class ListMetadataViewController: UIViewController {
    var interactor: ListMetadataBusinessLogic?
    var router: (NSObjectProtocol & ListMetadataRoutingLogic & ListMetadataDataPassing)?
    var index = 0

    private var fieldsAndAnswers: [FieldAndAnswer] = []
    private var keyboardSizeHieght: CGFloat?

    // MARK: - Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: - Setup

    private func setup() {
        let viewController = self
        let interactor = ListMetadataInteractor()
        let presenter = ListMetadataPresenter()
        let router = ListMetadataRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.backgroud
        addSubViews()
        setConstraints()
        setNavigationBar()
        getMetadata()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        updateAnswerButton()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillShowNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillHideNotification,
                                                  object: nil)
    }

    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]
                               as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }

    // MARK: - Setup SubViews

    @objc private func keyboardWillHide(notification: NSNotification) {
        tableView.contentInset = .zero
    }

    private func addSubViews() {
        view.addSubview(footerImageView)
        view.addSubview(tableView)
        view.addSubview(activityIndicatorView)
    }

    private func setConstraints() {
        tableView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.top.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }

        footerImageView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.top.bottom.equalToSuperview()
        }

        activityIndicatorView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    private func setNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.backgroundColor = ConstantColor.backgroud
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
    }

    // MARK: - UIElements

    private lazy var sendButton: UIButton = {
        let sendButton = UIButton()
        sendButton.setTitleColor(ConstantColor.defaultText, for: .normal)
        sendButton.setImage(UIImage(named: ConstantString.sendImageButton), for: .normal)
        sendButton.backgroundColor = ConstantColor.sendButton
        sendButton.addTarget(self, action: #selector(touchUpInsideSendButton), for: .touchUpInside)
        return sendButton
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(MetadataTableViewCell.self,
                           forCellReuseIdentifier: ConstantString.contactTableViewCellId)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView? = sendButton
        tableView.backgroundColor = .clear
        return tableView
    }()

    private lazy var footerImageView: UIImageView = {
        let footerImageView = UIImageView()
        footerImageView.contentMode = .scaleAspectFit
        footerImageView.alpha = 0.6
        return footerImageView
    }()

    private lazy var activityIndicatorView = UIActivityIndicatorView(style: .large)

    // MARK: - Get Metadata
    func getMetadata() {
        let request = ListMetadata.Fetch.Request()
        interactor?.getMetadata(request: request)
        showLoading()
    }

    private func updateAnswerButton() {
        let indexPath = IndexPath(row: index, section: 0)
        if nil != tableView.cellForRow(at: indexPath)?.contentView.subviews.last as? ValueButton {
            interactor?.updateMetadata(request: ListMetadata.Update.Request(indexField: index,
                                                                            answer: nil))
        }
    }

    // MARK: - Work ActivityIndicatorView
    private func showLoading() {
        isEditing = false
        view.bringSubviewToFront(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }

    private func hideLoading() {
        isEditing = true
        activityIndicatorView.stopAnimating()
    }

    // MARK: - Action UIElements
    @objc private func touchUpInsideValueButton(button: UIButton) {
        index = button.tag
        let valueController = ValueViewController()
        router?.routeToValue(valueViewController: valueController)
    }

    @objc func touchUpInsideSendButton(button: UIButton) {
        let answerViewController = AnswerViewController()
        view.endEditing(true)
        router?.routeToAnswer(answerViewController: answerViewController)
    }

    @objc func editingDidEndAnswerTextField(answerTextField: AnswerTextField) {
        fieldsAndAnswers[answerTextField.tag].field.answer = answerTextField.text
        let request = ListMetadata.Update.Request(indexField: answerTextField.tag,
                                                  answer: answerTextField.text)
        interactor?.updateMetadata(request: request)
    }

    @objc func editingChangedAnswerTextField(answerTextField: AnswerTextField) {
        fieldsAndAnswers[answerTextField.tag].field.answer = answerTextField.text
        let request = ListMetadata.Update.Request(indexField: answerTextField.tag,
                                                  answer: answerTextField.text)
        DispatchQueue.global(qos: .userInteractive).async {[weak self] in
            self?.interactor?.updateMetadata(request: request)
        }
    }
}

// MARK: - ListMetadataDisplayLogic
extension ListMetadataViewController: ListMetadataDisplayLogic {
    func displayFetch(viewModel: ListMetadata.Fetch.ViewModel) {
        title = viewModel.title
        footerImageView.image = viewModel.image
        fieldsAndAnswers = []
        for field in viewModel.fields {
            let fieldAndAnswerd = FieldAndAnswer(field: field,
                                                 color: ConstantColor.defaultText)
            fieldsAndAnswers.append(fieldAndAnswerd)
        }
        tableView.reloadData()
        hideLoading()
    }

    func displayUpdate(viewModel: ListMetadata.Update.ViewModel) {
        fieldsAndAnswers[viewModel.indexField].color = viewModel.color
        let indexPath = IndexPath(row: viewModel.indexField, section: 0)

        if let views = tableView.cellForRow(at: indexPath)?.contentView.subviews {
            for view in views {
                if let view = view as? AnswerTextField {
                    view.textColor = viewModel.color
                } else if let view = view as? ValueButton {
                    fieldsAndAnswers[view.tag].field.answer = viewModel.answer
                    view.setTitle(viewModel.answer, for: .normal)
                }
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension ListMetadataViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        sendButton
    }
}

// MARK: - UITableViewDataSource
extension ListMetadataViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        fieldsAndAnswers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConstantString.contactTableViewCellId,
                                                 for: indexPath) as? MetadataTableViewCell
        guard let cell = cell else { return MetadataTableViewCell()}
        cell.titleLable.text = fieldsAndAnswers[indexPath.row].field.title
        cell.setAnswerView(fieldsAndAnswers[indexPath.row].field.answer,
                           fieldsAndAnswers[indexPath.row].field.type,
                           indexPath.row,
                           fieldsAndAnswers[indexPath.row].color)
        switch fieldsAndAnswers[indexPath.row].field.type {
        case .text:
            cell.answerTextField.addTarget(self,
                                           action: #selector(editingChangedAnswerTextField),
                                           for: .editingChanged)
        case .numeric:
            cell.answerTextField.addTarget(self,
                                           action: #selector(editingDidEndAnswerTextField),
                                           for: .editingDidEnd)
        case .list:
            cell.valueButton.addTarget(self, action: #selector(touchUpInsideValueButton),
                                       for: .touchUpInside)
        }
        return cell
    }
}
