//
//  ListMetadataPresenter.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit

// MARK: - Protocols
protocol ListMetadataPresentationLogic {
    func presentFetchMetadata(response: ListMetadata.Fetch.Response)
    func presentUpdateMetadata(response: ListMetadata.Update.Response)
}

// MARK: - ListMetadataPresenter
class ListMetadataPresenter {
    weak var viewController: ListMetadataDisplayLogic?

    private func createViewModel(response: ListMetadata.Fetch.Response) -> ListMetadata.Fetch.ViewModel {
        var fields: [ListMetadata.Field] = []
        for field in response.metadata.fields {
            let viewModelField = ListMetadata.Field(title: field.title,
                                                    type: field.type,
                                                    answer: field.answer)
            fields.append(viewModelField)
        }
        let viewModel = ListMetadata.Fetch.ViewModel(title: response.metadata.title,
                                                     image: response.image,
                                                     fields: fields)
        return viewModel
    }
}

// MARK: - ListMetadataPresentationLogic
extension ListMetadataPresenter: ListMetadataPresentationLogic {
    func presentFetchMetadata(response: ListMetadata.Fetch.Response) {
        let viewModel = createViewModel(response: response)
        viewController?.displayFetch(viewModel: viewModel)
    }
    func presentUpdateMetadata(response: ListMetadata.Update.Response) {
        let color = response.isCorrectAnswer ? ConstantColor.good : ConstantColor.bad
        let viewModel = ListMetadata.Update.ViewModel(indexField: response.indexField,
                                                      color: color,
                                                      answer: response.answer)
        viewController?.displayUpdate(viewModel: viewModel)
    }
}
