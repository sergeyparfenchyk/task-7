//
//  ListMetadataModels.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit

enum ListMetadata {

    // MARK: Field
    struct Field {
        let title: String
        let type: Metadata.TypeField
        var answer: String?
    }

    // MARK: Fetch
    enum Fetch {
        struct Request {
        }
        struct Response {
            let metadata: Metadata
            let image: UIImage?
        }
        struct ViewModel {
            let title: String
            let image: UIImage?
            let fields: [Field]
        }
    }

    // MARK: Update
    enum Update {
        struct Request {
            let indexField: Int
            let answer: String?
        }
        struct Response {
            let indexField: Int
            let isCorrectAnswer: Bool
            let answer: String?
        }
        struct ViewModel {
            let indexField: Int
            let color: UIColor
            var answer: String?
        }
    }
}
