//
//  ListMetadataRouter.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit

// MARK: - Protocols

@objc protocol ListMetadataRoutingLogic {
    func routeToValue(valueViewController: ValueViewController)
    func routeToAnswer(answerViewController: AnswerViewController)
}

protocol ListMetadataDataPassing {
    var dataStore: ListMetadataDataStore? { get }
}

// MARK: - ListMetadataRouter
class ListMetadataRouter: NSObject, ListMetadataRoutingLogic, ListMetadataDataPassing {
    weak var viewController: ListMetadataViewController?
    var dataStore: ListMetadataDataStore?

    // MARK: Routing
    func routeToValue(valueViewController: ValueViewController) {
        guard var destinationDS = valueViewController.router?.dataStore else { return }
        passDataToValue(source: dataStore, destination: &destinationDS)
        if let viewController = viewController {
            navigateToValue(source: viewController, destination: valueViewController)
        }
    }

    func routeToAnswer(answerViewController: AnswerViewController) {
        guard var destinationDS = answerViewController.router?.dataStore else { return }
        passDataToAnswer(source: dataStore, destination: &destinationDS)
        if let viewController = viewController {
            navigateToAnswer(source: viewController, destination: answerViewController)
        }
    }

    // MARK: Navigation
    func navigateToValue(source: ListMetadataViewController, destination: ValueViewController) {
        source.show(destination, sender: nil)
    }

    func navigateToAnswer(source: ListMetadataViewController, destination: AnswerViewController) {
        source.show(destination, sender: nil)
    }

    // MARK: Passing data
    func passDataToValue(source: ListMetadataDataStore?, destination: inout ValueDataStore) {
        guard let index = viewController?.index else { return }
        if let metadata = source?.metadata {
            let values = metadata.fields[index].sortedValues
            if let values = values {
                destination.values = values
                destination.indexValue = index
            }
        }
    }

    func passDataToAnswer(source: ListMetadataDataStore?, destination: inout AnswerDataStore) {
        if let metadata = source?.metadata {
            for field in metadata.fields {
                if let answer = field.answer {
                    destination.metadataPost.form[field.name] = answer
                }
            }
        }
    }
}
