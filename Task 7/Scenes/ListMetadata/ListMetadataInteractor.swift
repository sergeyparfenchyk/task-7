//
//  ListMetadataInteractor.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit
import Network

// MARK: - Protocols

protocol ListMetadataBusinessLogic {
    func getMetadata(request: ListMetadata.Fetch.Request)
    func updateMetadata(request: ListMetadata.Update.Request)
}

protocol ListMetadataDataStore {
    var metadata: Metadata? { get set}
    var image: UIImage? { get set }
    var answer: MetadataPost { get set }
}

// MARK: - ListMetadataInteractor
class ListMetadataInteractor: ListMetadataDataStore {
    var metadata: Metadata?
    var image: UIImage?
    var answer = MetadataPost(form: [:])

    var presenter: ListMetadataPresentationLogic?
    private(set) var networkWorker: ListMetadataNetworkWorker?
    private(set) var validationkWorker: ListMetadaValidationWorker?

    // MARK: Fetch Metadata

    private func fetchMetadata(request: ListMetadata.Fetch.Request) {
        networkWorker = ListMetadataNetworkWorker()
        networkWorker?.fetchMetadata(on: Metadata.self, result: {[weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure:
                self.metadata = Metadata(title: "", image: "", fields: [])
            case .success((let data, let image)):
                let someData = data.fields
                let tmpData = Metadata(title: data.title, image: data.image, fields: someData)
                self.metadata = tmpData
                self.image = image
                self.setDefaultAnswerts()
            }
            self.presentMetadata()
        })
    }

    private func presentMetadata() {
        if let metadata = self.metadata {
            let response = ListMetadata.Fetch.Response(metadata: metadata, image: self.image)
            self.presenter?.presentFetchMetadata(response: response)
        }
    }

    private func setDefaultAnswerts() {
        if let fields = self.metadata?.fields {
            for (index, field) in fields.enumerated() {
                self.metadata?.fields[index].answer = field.chasedValue
            }
        }
    }
}

// MARK: - ListMetadataBusinessLogic
extension ListMetadataInteractor: ListMetadataBusinessLogic {
    func getMetadata(request: ListMetadata.Fetch.Request) {
        MonitorNetworkWorker.checkInternet {[weak self] isInternet in
            guard let self = self else { return }
            if isInternet {
                self.fetchMetadata(request: request)
            } else {
                self.metadata = Metadata(title: "", image: "", fields: [])
                self.presentMetadata()
            }
        }
    }

    func updateMetadata(request: ListMetadata.Update.Request) {
        let validationWorker = ListMetadaValidationWorker()
        guard let metadata = self.metadata else {
            return
        }
        var possibleAnswer = request.answer
        let type = metadata.fields[request.indexField].type
        var isCorrectAnswer = true
        switch type {
        case .text:
            isCorrectAnswer = validationWorker.isGoodText(possibleAnswer)
        case .numeric:
            isCorrectAnswer = validationWorker.isGoodNumber(possibleAnswer)
        case .list:
            possibleAnswer = self.metadata?.fields[request.indexField].answer
            isCorrectAnswer = true
        }
        self.metadata?.fields[request.indexField].answer = isCorrectAnswer ? possibleAnswer : nil
        let response = ListMetadata.Update.Response(indexField: request.indexField,
                                                    isCorrectAnswer: isCorrectAnswer,
                                                    answer: self.metadata?.fields[request.indexField].answer)
        DispatchQueue.main.async { [weak self] in
            self?.presenter?.presentUpdateMetadata(response: response)
        }
    }
}
