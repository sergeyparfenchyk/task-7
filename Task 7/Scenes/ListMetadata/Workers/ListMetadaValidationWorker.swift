//
//  ListMetadaValidationWorker.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 21.03.22.
//

import UIKit

class ListMetadaValidationWorker {
    func isGoodNumber(_ number: String?) -> Bool {
        guard let number = number else {
            return false
        }
        guard let number = Float(number) else {
            return false
        }
        guard number > 1 && number < 1024 else {
            return false
        }
        return true
    }

    func isGoodText(_ text: String?) -> Bool {
        guard let text = text else {
            return false
        }
        let textCheking = text.trimmingCharacters(in: CharacterSet.whitespaces)
        let regex = "[a-zA-Zа-яА-Я0-9]{1,}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        let result = predicate.evaluate(with: textCheking)
        return result
    }
}
