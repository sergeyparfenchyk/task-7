//
//  ListMetadataNetworkWorker.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit
import Combine

class ListMetadataNetworkWorker {
    var subscriptions: Set<AnyCancellable> = []

    // MARK: - Fetch Metadata
    func fetchMetadata<T: RequestableMetadata>(on decodable: T.Type,
                                               result: @escaping (Result<(T, UIImage?), Error>) -> Void) {
        let metadataPublisher = URLSession.shared.dataTaskPublisher(for: decodable.urlRequest)
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()

        let imagePublisher = metadataPublisher
            .flatMap { metadata -> AnyPublisher<UIImage?, Error> in
                return self.fetchImage(stringUrl: metadata.image)
            }
            .collect()
            .eraseToAnyPublisher()

        Publishers.Zip(metadataPublisher, imagePublisher)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        result(.failure(error))
                    case .finished:
                        break
                    }
                },
                receiveValue: { metadata, image in
                    result(.success((metadata, image.first as? UIImage)))
                }
            )
            .store(in: &subscriptions)
    }

    // MARK: - Fetch Image
    private func fetchImage(stringUrl: String) -> AnyPublisher<UIImage?, Error> {
        guard let url = URL(string: stringUrl) else {
            return Result.Publisher(nil).eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: url)
            .compactMap { UIImage(data: $0.data) }
            .mapError { $0 as Error}
            .eraseToAnyPublisher()
    }
}
