//
//  TitleLable.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 17.03.22.
//

import UIKit

// MARK: - Constants
private extension ConstantSize {
    static let numberOfLines = 0
}

// MARK: - TitleLable
class TitleLable: UILabel {

    // MARK: - Initialize

    init() {
        super.init(frame: .zero)
        numberOfLines = ConstantSize.numberOfLines
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
