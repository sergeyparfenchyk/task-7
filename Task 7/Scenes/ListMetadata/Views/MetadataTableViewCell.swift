//
//  MetadataTableViewCell.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 17.03.22.
//

import UIKit

// MARK: - Constants
private extension ConstantSize {
    static let trailingAndLeadingTitleAndAnswer = 15.0
    static let topAndBottomTitleAndAnswer = 10.0
}

// MARK: - MetadataTableViewCell
class MetadataTableViewCell: UITableViewCell {

    // MARK: - UIElements
    private(set) lazy var titleLable = TitleLable()
    private(set) lazy var valueButton = ValueButton()
    private(set) lazy var answerTextField = AnswerTextField()

    // MARK: - Initialize

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        backgroundColor = .clear
        selectionStyle = .none
        addSubviews()
        setConstraints()
    }

    private func addSubviews() {
        contentView.addSubview(titleLable)
    }

    private func setConstraints() {
        titleLable.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(ConstantSize.topAndBottomTitleAndAnswer)
            make.bottom.equalToSuperview().offset(-ConstantSize.topAndBottomTitleAndAnswer)
            make.leading.equalToSuperview().offset(ConstantSize.trailingAndLeadingTitleAndAnswer)
        }
    }

    // MARK: - Setup Answer view

    func setAnswerView(_ answer: String?, _ type: Metadata.TypeField, _ tag: Int, _ color: UIColor) {
        switch type {
        case .text:
            answerTextField.typeField = .text
            setAnswerTextField(answer, tag, color)
        case .numeric:
            answerTextField.typeField = .numeric
            setAnswerTextField(answer, tag, color)
        case .list:
            addAnswerSubview(valueButton)
            setConstraintsAnswerView(valueButton)
            valueButton.setTitle(answer, for: .normal)
            valueButton.tag = tag
        }
    }

    private func setAnswerTextField(_ answer: String?, _ tag: Int, _ color: UIColor) {
        addAnswerSubview(answerTextField)
        setConstraintsAnswerView(answerTextField)
        answerTextField.tag = tag
        answerTextField.text = answer
        answerTextField.textColor = color
    }

    private func setConstraintsAnswerView(_ view: UIView) {
        view.snp.makeConstraints { make in
            make.top.equalTo(titleLable.snp.top)
            make.bottom.equalTo(titleLable.snp.bottom)
            make.trailing.equalToSuperview().offset(-ConstantSize.trailingAndLeadingTitleAndAnswer)
            make.leading.equalTo(titleLable.snp.trailing)
            make.width.equalTo(titleLable.snp.width)
        }
    }

    private func addAnswerSubview(_ view: UIView) {
        contentView.addSubview(view)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        if let lastView = contentView.subviews.last {
            lastView.removeFromSuperview()
        }
    }
}
