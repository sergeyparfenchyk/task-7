//
//  ValueButton.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 17.03.22.
//

import UIKit

// MARK: - Constants
private extension ConstantColor {
    static let titleColor = UIColor.label
}

// MARK: - ValueButton
class ValueButton: UIButton {

    // MARK: - Initialize

    init() {
        super.init(frame: .zero)
        setTitleColor(ConstantColor.titleColor, for: .normal)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
