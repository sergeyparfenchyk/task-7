//
//  AnswerTextField.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 17.03.22.
//

import UIKit

// MARK: - Constants
private extension ConstantString {
    static let placeholderText = "..."
    static let placeholderNumber = "1"
}

// MARK: - AnswerTextField
class AnswerTextField: UITextField {

    // MARK: - Public properties
    var typeField: Metadata.TypeField = .text {
        didSet {
            changeKeyboardType()
        }
    }

    // MARK: - Initialize
    init() {
        super.init(frame: .zero)
        self.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Keyboard Type
    private func changeKeyboardType() {
        switch typeField {
        case .text:
            keyboardType = .default
            placeholder = ConstantString.placeholderText
        case .numeric:
            keyboardType = .numbersAndPunctuation
            placeholder = ConstantString.placeholderNumber
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate
extension AnswerTextField: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
        return false
    }
}
