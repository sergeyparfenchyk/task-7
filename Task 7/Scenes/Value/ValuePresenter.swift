//
//  ValuePresenter.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 18.03.22.
//

import UIKit

// MARK: - Protocols

protocol ValuePresentationLogic {
    func presentValues(response: Value.List.Response)
}

// MARK: - ValuePresenter
class ValuePresenter {
    weak var viewController: ValueDisplayLogic?
}

// MARK: - ValuePresentationLogic
extension ValuePresenter: ValuePresentationLogic {
    func presentValues(response: Value.List.Response) {
        let viewModel = Value.List.ViewModel(values: response.values)
        viewController?.displayValues(viewModel: viewModel)
    }
}
