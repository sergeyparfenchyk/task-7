//
//  ValueInteractor.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 18.03.22.
//

import UIKit

// MARK: - Protocols

protocol ValueBusinessLogic {
    func getValues(request: Value.List.Request)
}

protocol ValueDataStore {
    var values: [String] { get set }
    var indexValue: Int { get set }
}

// MARK: - ValueInteractor
class ValueInteractor: ValueDataStore {
    var values: [String] = []
    var indexValue = 0
    var presenter: ValuePresentationLogic?

    private var worker: ValueWorker?
}

// MARK: - ValueBusinessLogic
extension ValueInteractor: ValueBusinessLogic {
    func getValues(request: Value.List.Request) {
        worker = ValueWorker()
        let response = Value.List.Response(values: values)
        presenter?.presentValues(response: response)
    }
}
