//
//  ValueModels.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 18.03.22.
//

import UIKit

enum Value {

    // MARK: List
    enum List {
        struct Request {
        }
        struct Response {
            var values: [String]
        }
        struct ViewModel {
            var values: [String]
        }
    }
}
