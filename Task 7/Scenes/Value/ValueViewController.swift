//
//  ValueViewController.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 18.03.22.
//

import UIKit

// MARK: - Constats
private extension ConstantString {
    static let cellId = "cellId"
}

// MARK: - Protocols
protocol ValueDisplayLogic: AnyObject {
    func displayValues(viewModel: Value.List.ViewModel)
}

// MARK: - ValueViewController
class ValueViewController: UIViewController {
    var interactor: ValueBusinessLogic?
    var router: (NSObjectProtocol & ValueRoutingLogic & ValueDataPassing)?
    var values: [String] = []

    // MARK: - Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = ValueInteractor()
        let presenter = ValuePresenter()
        let router = ValueRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        setConstraint()
        getData()
    }

    // MARK: - Setup SubViews

    private func addSubviews() {
        view.addSubview(valueTableView)
        view.backgroundColor = ConstantColor.backgroud
    }

    private func setConstraint() {
        valueTableView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.top.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }

    // MARK: - UIElements
    private(set) lazy var valueTableView: UITableView = {
        let valueTableView = UITableView()
        valueTableView.delegate = self
        valueTableView.dataSource = self
        valueTableView.register(ValueTableViewCell.self, forCellReuseIdentifier: ConstantString.cellId)
        return valueTableView
    }()

    // MARK: - Get data
    func getData() {
        let request = Value.List.Request()
        interactor?.getValues(request: request)
    }
}

// MARK: - ValueDisplayLogic
extension ValueViewController: ValueDisplayLogic {
    func displayValues(viewModel: Value.List.ViewModel) {
        values = viewModel.values
    }
}

// MARK: - UITableViewDelegate
extension ValueViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.popViewController(animated: true)
        router?.routeToList()
    }
}

// MARK: - UITableViewDataSource
extension ValueViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let valueCell = tableView.dequeueReusableCell(withIdentifier: ConstantString.cellId,
                                                      for: indexPath) as? ValueTableViewCell
        guard let valueCell = valueCell else { return ValueTableViewCell() }
        valueCell.titleLable.text = values[indexPath.row]
        return valueCell
    }
}
