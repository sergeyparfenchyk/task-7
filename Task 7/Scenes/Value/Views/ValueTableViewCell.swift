//
//  ValueTableViewCell.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 18.03.22.
//

import UIKit

// MARK: - Constants
private extension ConstantSize {
    static let leadingAndTralingOffset = 15.0
    static let topAndBottomOffset = 10.0
}

// MARK: - ValueTableViewCell
class ValueTableViewCell: UITableViewCell {

    // MARK: - UIElements
    private(set) lazy var titleLable: UILabel = {
        let titleLable = UILabel()
        titleLable.numberOfLines = 20
        return titleLable
    }()

    // MARK: - Initialize
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        addSubviews()
        setConstraint()
    }

    private func addSubviews() {
        addSubview(titleLable)
    }

    private func setConstraint() {
        titleLable.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(ConstantSize.leadingAndTralingOffset)
            make.trailing.equalToSuperview().offset(-ConstantSize.leadingAndTralingOffset)
            make.top.equalToSuperview().offset(ConstantSize.topAndBottomOffset)
            make.bottom.equalToSuperview().offset(-ConstantSize.topAndBottomOffset)
        }
    }
}
