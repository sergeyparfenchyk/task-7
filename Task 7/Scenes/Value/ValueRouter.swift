//
//  ValueRouter.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 18.03.22.
//

import UIKit

// MARK: - Protocols

@objc protocol ValueRoutingLogic {
    func routeToList()
}

protocol ValueDataPassing {
    var dataStore: ValueDataStore? { get }
}

// MARK: - ValueRouter
class ValueRouter: NSObject, ValueRoutingLogic, ValueDataPassing {
    weak var viewController: ValueViewController?
    var dataStore: ValueDataStore?

    // MARK: - Routing to list
    func routeToList() {
        guard var index = viewController?.navigationController?.viewControllers.count else { return }
        index -= 1
        if let destinationVC = viewController?.navigationController?.viewControllers[index]
            as? ListMetadataViewController {
            guard var destinationDS = destinationVC.router?.dataStore else { return }
            passDataToList(source: dataStore, destination: &destinationDS)
            navigateToList(source: viewController, destination: destinationVC) }
    }

    // MARK: - Navigation to list
    func navigateToList(source: ValueViewController?, destination: ListMetadataViewController) {
        source?.navigationController?.popViewController(animated: true)
    }

    // MARK: - Passing data to list
    func passDataToList(source: ValueDataStore?, destination: inout ListMetadataDataStore) {
        let indexChasedValue = viewController?.valueTableView.indexPathForSelectedRow?.row
        guard let index = source?.indexValue else { return }
        destination.metadata?.fields[index].indexChasedValue = indexChasedValue
        if let sortedValues = destination.metadata?.fields[index].sortedValues,
           let indexChasedValue = indexChasedValue {
            destination.metadata?.fields[index].answer = sortedValues[indexChasedValue]
        }
    }
}
