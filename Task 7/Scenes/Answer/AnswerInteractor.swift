//
//  AnswerInteractor.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import UIKit

// MARK: - Protocols

protocol AnswerBusinessLogic {
    func postAnswers(request: Answer.Post.Request)
}

protocol AnswerDataStore {
    var metadataPost: MetadataPost { get set }
    var metadataAnswer: MetadataAnswer { get set }
}

// MARK: - AnswerInteractor
class AnswerInteractor: AnswerDataStore {
    var metadataPost = MetadataPost(form: [ : ])
    var metadataAnswer = MetadataAnswer(result: "")
    var presenter: AnswerPresentationLogic?

    private var worker: AnswerWorker?
}

// MARK: - AnswerBusinessLogic
extension AnswerInteractor: AnswerBusinessLogic {
    func postAnswers(request: Answer.Post.Request) {
        MonitorNetworkWorker.checkInternet {[weak self] isInternet in
            guard let self = self else { return }
            if isInternet {
                self.submitAnswers(request: request)
            } else {
                let response = Answer.Post.Response(answer: self.metadataAnswer)
                self.presenter?.presentAnswer(response: response)
            }
        }
    }

    private func submitAnswers(request: Answer.Post.Request) {
        worker = AnswerWorker()
        worker?.submitAnswers(on: MetadataAnswer.self,
                              answer: metadataPost,
                              result: {[weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure:
                self.metadataAnswer = MetadataAnswer(result: "")
            case .success((let data)):
                self.metadataAnswer = data
            }
            let response = Answer.Post.Response(answer: self.metadataAnswer)
            self.presenter?.presentAnswer(response: response)
        })
    }
}
