//
//  AnswerViewController.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import UIKit

// MARK: Constants
private extension ConstantSize {
    static let leadingAndTralingOffset = 10.0
    static let topAndBottomOffsset = 10.0
}

// MARK: - Protocols

protocol AnswerDisplayLogic: AnyObject {
    func displayAnswer(viewModel: Answer.Post.ViewModel)
}

// MARK: - AnswerViewController
class AnswerViewController: UIViewController {
    var interactor: AnswerBusinessLogic?
    var router: (NSObjectProtocol & AnswerRoutingLogic & AnswerDataPassing)?

    // MARK: - Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: - Setup

    private func setup() {
        let viewController = self
        let interactor = AnswerInteractor()
        let presenter = AnswerPresenter()
        let router = AnswerRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.backgroud
        addSubviews()
        setConstraints()
        postAnswers()
    }

    // MARK: Setup SubViews

    private func addSubviews() {
        view.addSubview(answerScrollView)
        answerScrollView.addSubview(answerLable)
        view.addSubview(activityIndicatorView)
    }

    private func setConstraints() {
        answerScrollView.snp.makeConstraints { make in
            make.trailing.leading.equalTo(view.safeAreaLayoutGuide)
            make.top.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }

        answerLable.snp.makeConstraints { make in
            make.trailing.leading.equalTo(view.safeAreaLayoutGuide).offset(ConstantSize.leadingAndTralingOffset)
            make.top.bottom.equalToSuperview()
        }
        activityIndicatorView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    // MARK: - UIElements
    private lazy var activityIndicatorView = UIActivityIndicatorView(style: .large)
    private lazy var answerScrollView = UIScrollView()
    private lazy var answerLable: UILabel = {
        let answerLable = UILabel()
        answerLable.numberOfLines = 0
        return answerLable
    }()

    // MARK: - Post Answers
    private func postAnswers() {
        let request = Answer.Post.Request()
        interactor?.postAnswers(request: request)
        showLoading()
    }

    // MARK: - Work ActivityIndicatorView

    private func showLoading() {
        isEditing = false
        activityIndicatorView.startAnimating()
        view.bringSubviewToFront(activityIndicatorView)
    }

    private func hideLoading() {
        isEditing = true
        activityIndicatorView.stopAnimating()
    }
}

// MARK: AnswerDisplayLogic
extension AnswerViewController: AnswerDisplayLogic {
    func displayAnswer(viewModel: Answer.Post.ViewModel) {
        DispatchQueue.main.async {[weak self] in
            guard let self = self else { return }
            self.answerLable.text = viewModel.answer
            self.hideLoading()
        }
    }
}
