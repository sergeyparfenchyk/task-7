//
//  AnswerRouter.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import UIKit

// MARK: - Protocols

@objc protocol AnswerRoutingLogic {
}

protocol AnswerDataPassing {
    var dataStore: AnswerDataStore? { get }
}

// MARK: - AnswerRouter
class AnswerRouter: NSObject, AnswerRoutingLogic, AnswerDataPassing {
    weak var viewController: AnswerViewController?
    var dataStore: AnswerDataStore?
}
