//
//  AnswerModels.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import UIKit

enum Answer {
    // MARK: - Post
    enum Post {
        struct Request {
        }
        struct Response {
            var answer: MetadataAnswer
        }
        struct ViewModel {
            var answer: String?
        }
    }
}
