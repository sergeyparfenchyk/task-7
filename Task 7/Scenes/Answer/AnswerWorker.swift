//
//  AnswerWorker.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import UIKit
import Combine

// MARK: - AnswerWorker
class AnswerWorker {
    var subscriptions: Set<AnyCancellable> = []
    func submitAnswers<T: ResponsibleMetadata>(on decodable: T.Type,
                                               answer: MetadataPost,
                                               result: @escaping (Result<T, Error>) -> Void) {
        var urlRequest = decodable.urlRequest
        let jsonData = try? JSONEncoder().encode(answer)
        urlRequest.httpBody = jsonData
        URLSession.shared.dataTaskPublisher(for: urlRequest)
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    result(.failure(error))
                }
            } receiveValue: { data in
                result(.success(data))
            }
            .store(in: &subscriptions)
    }
}
