//
//  AnswerPresenter.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 20.03.22.
//

import UIKit

// MARK: - Protocols
protocol AnswerPresentationLogic {
    func presentAnswer(response: Answer.Post.Response)
}

// MARK: - AnswerPresenter
class AnswerPresenter {
    weak var viewController: AnswerDisplayLogic?
}

// MARK: - AnswerPresentationLogic
extension AnswerPresenter: AnswerPresentationLogic {
    func presentAnswer(response: Answer.Post.Response) {
        let viewModel = Answer.Post.ViewModel(answer: response.answer.result)
        viewController?.displayAnswer(viewModel: viewModel)
    }
}
