//
//  ConstantColor.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 15.03.22.
//

import UIKit

struct ConstantColor {
    static let backgroud = UIColor.systemBackground
    static let good = UIColor.systemGreen
    static let bad = UIColor.systemRed
    static let defaultText = UIColor.label
}
