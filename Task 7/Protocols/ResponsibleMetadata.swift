//
//  ResponsibleMetadata.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 21.03.22.
//

import Foundation

protocol ResponsibleMetadata: Codable {
    static var urlRequest: URLRequest { get }
}
