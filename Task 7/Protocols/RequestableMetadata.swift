//
//  RequestableMetadata.swift
//  Task 7
//
//  Created by Sergey Parfentchik on 21.03.22.
//

import Foundation

protocol RequestableMetadata: Codable {
    static var urlRequest: URL { get }
    var image: String { get }
}
